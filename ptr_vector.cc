#include <cassert>
#include <memory>
#include <vector>

/* Vector with each element allocated separately, so that pointers to element
 * are not invalidated on resize.
 *
 * Typically used as a replacement for linked lists when random access is
 * needed and removing individual elements does not need to be efficient. */
template<typename T> class PtrVector {
 protected:
  std::vector<std::unique_ptr<T>> data;

 public:
  /* Basic iterators for range based for loop. */
  struct ConstIterator {
    typename std::vector<std::unique_ptr<T>>::const_iterator it;

    const T &operator*() const
    {
      return *(it->get());
    }
    bool operator!=(const ConstIterator &other) const
    {
      return it != other.it;
    }
    void operator++()
    {
      ++it;
    }
  };

  struct Iterator {
    typename std::vector<std::unique_ptr<T>>::const_iterator it;

    T &operator*() const
    {
      return *(it->get());
    }
    bool operator!=(const Iterator &other) const
    {
      return it != other.it;
    }
    void operator++()
    {
      ++it;
    }
  };

  T &operator[](const size_t i) const
  {
    return *(data[i].get());
  }

  void append(T &&value)
  {
    data.push_back(std::make_unique<T>(std::move(value)));
  }

  void append(std::unique_ptr<T> &&value)
  {
    assert(value.get() != nullptr);
    data.push_back(std::move(value));
  }

  bool is_empty() const
  {
    return data.empty();
  }

  size_t size() const
  {
    return data.size();
  }

  void clear()
  {
    data.clear();
  }

  ConstIterator begin() const
  {
    return ConstIterator{data.begin()};
  }
  ConstIterator end() const
  {
    return ConstIterator{data.end()};
  }

  Iterator begin()
  {
    return Iterator{data.begin()};
  }
  Iterator end()
  {
    return Iterator{data.end()};
  }

    void erase(const size_t i)
  {
    data.erase(data.begin() + i);
  }

  void erase(const ConstIterator &it)
  {
    data.erase(it.it);
  }

  void erase(const Iterator &it)
  {
    data.erase(it.it);
  }
};

struct Data {
  int a = 1, b = 2, c = 3;
};

int main()
{
  PtrVector<Data> vec;
  vec.append(std::make_unique<Data>());
  vec.append({4, 5, 6});

  for (Data &data : vec) {
    printf("%d\n", data.a);
  }
  for (const Data &data : vec) {
    printf("%d\n", data.b);
  }

  vec.erase(1);

  return 0;
}