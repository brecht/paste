float musgrave_combined(
    float co, float H, float lacunarity, float octaves, float offset = 0.0f, float gain = 1.0f)
{
  const float pwHL = powf(lacunarity, -H);
  const float rmd = octaves - floorf(octaves);
  const int iterations = float_to_int(ceilf(octaves));

  float p = co;
  float pwr = 1.0f;
  float weight = 1.0f;
  float value = 0.0f;

  for (int i = 0; i < iterations; i++) {
    float signal;

    if (RIDGED_MULTI_FRACTAL) {
      signal = sqr(-offset + fabsf(snoise_1d(p)));
    }
    else {
      signal = offset + snoise_1d(p);
    }

    signal *= pwr;
    signal *= weight;

    if (i + 1 < iterations) {
      value += signal;

      pwr *= pwHL;
      p *= lacunarity;

      if (RIDGED_MULTI_FRACTAL) {
        weight = minf(gain * signal, 1.0f);
      }
      else if (HYBRID_MULTI_FRACTAL) {
        weight = minf(weight * gain * signal, 1.0f);
      }
      else {
        weight = minf(weight * gain, 1.0f) * value;
      }
    }
    else if (rmd == 0.0f) {
      value += signal;
    }
    else {
      value += rmd * signal;
    }
  }

  if (MULTI_FRACTAL_1D) {
    value += 1.0f;
  }

  return value;
}

