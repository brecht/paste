# HIP SDK
#
# Install just a subset to avoid having to install kernel drivers.
#
sudo sh -c 'echo "[ROCm]
name=ROCm
baseurl=https://repo.radeon.com/rocm/yum/5.5/main
enabled=1
gpgcheck=1
exclude=rock-dkms
gpgkey=https://repo.radeon.com/rocm/rocm.gpg.key" > /etc/yum.repos.d/rocm.repo'

sudo rpm --import https://repo.radeon.com/rocm/rocm.gpg.key

sudo yum install -y hip-devel rocm-llvm rocm-core rocm-device-libs5.5.0.x86_64