from bpy.types import NODE_MT_shader_node_add_all

original_draw = NODE_MT_shader_node_add_all.draw

def my_draw(self, context):
    if context.scene.render.engine == 'BLENDER_WORKBENCH':
        self.layout.label(text="Hello world")
    else:
        original_draw(self, context)

NODE_MT_shader_node_add_all.draw = my_draw