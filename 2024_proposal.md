# 2024

<!--
Keep the order and titles unchanged, so the this can be parsed automatically.
Uncomment as you create reports, so that the most recent entry is at the top.
-->

<!--

## December 23 - 29

## December 16 - 22

## December 9 - 15

## December 2 - 8

## November 25 - December 1

## November 18 - 24

## November 11 - 17

## November 4 - 10

## October 28 - November 3

## October 21 - 27

## October 14 - 20

## October 7 - 13

## September 30 - October 6

## September 23 - 29

## September 16 - 22

## September 9 - 15

## September 2 - 8

## August 26 - September 1

## August 19 - 25

## August 12 - 18

## August 5 - 11

## July 29 - August 4

## July 22 - 28

## July 15 - 21

## July 8 - 14

## July 1 - 7

## June 24 - 30

## June 17 - 23

## June 10 - 16

## June 3 - 9

## May 27 - June 2

## May 20 - 26

## May 13 - 19

## May 6 - 12

## April 29 - May 5

## April 22 - 28

## April 15 - 21

## April 8 - 14

## April 1 - 7

## March 25 - 31

## March 18 - 24

## March 11 - 17

## March 4 - 10

## February 26 - March 3

## February 19 - 25

## February 12 - 18

## February 5 - 11

## January 29 - February 4

## January 22 - 28

## January 15 - 21

## January 8 - 14

-->

## January 1 - 7
