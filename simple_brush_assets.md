Proposal to simplify the brush assets design, to more efficiently wrap up the project.

## Storage

No separate drafts storage, everything goes to the default user library. Asset libraries in the preferences get a boolean indicating which is the default library to save to.

New brush assets are saved in `<User Library>/Saved/Brushes/brush_name.asset.blend`. When saving the asset, only a single datablock and its dependencies are saved, no workspaces or scenes. These types of `.asset.blend` files are the ones where assets can be updated and deleted.

Additional datablocks used by the asset, like textures and images, are also pushed to this file. Images will be packed, to make the asset file self contained.

## Editing Considerations

Updating and deleting assets is not undoable. Therefore at least deleting should show a confirmation popup. Updating could get a popup when updating by dragging an asset on top of an existing one in the asset browser, as there is some risk of doing this accidentally.

Update and delete operations for regular blend files would be disabled, and show a tooltip or dialog like the following:
> This is a read-only asset from characters/bob/bob.blend.  
> It can be edited by opening the file.

Arguably, if a blend file was placed in the user library (not project library), its only purpose is to contain appendable assets and it should be editable, so this could be considered for a following iteration.

If we support pushing assets to arbitrary blend files, we should clearly communicate the consequences and ask for confirmation. For example a dialog like:
> Update Asset?  
> This will modify the asset in characters/bob/bob.blend.  
> The entire file will be upgraded from Blender 3.6 to 4.2.

Delete asset would remove the asset metadata from the datablock, instead of deleting it.

Regardless of restrictions on pushing, we should communicate when a blend file that was opened in Blender is part of an asset library. The window title should show the asset library name, or alternatively the project name when support for that is added.

## Multiple Libraries

Essential and extenions add-ons would always be read-only. But other asset libraries in the preferences would support asset pushing.

To choose the asset library, there two ways of doing it:
* A single asset library is marked as default, to indicate the library to save brushes and other assets to.
* THe new and duplicate asset operators offer an option to choose which library to save to with a small sub menu, or popup menu for drag & drop.

We can get user feedback to figure out which is most convenient.

## Hide & Replace

Users may want to hide or replace brushes from the essentials asset library, or potentially other asset libraries. This may be because they want to use a different set of brushes than the default essentials, or because they want to change the default of one or two settings.

For this, every asset can be marked as hidden. Replace would be implemented in terms of hiding a brush and adding a new brush. The asset browsers would have a button show hidden assets, which would allow unhiding. 

The hiding state would be stored in the catalog file, along with the catalog that the asset is part of.

The user library catalog would need to be able to store hidding state for assets that are part of other asset libraries. Particularly the essentials library. Potentially this could also allow re-organizing the essentials assets into differnet catalogs.

Note that while this is important functionality, a first iteration of brush assets does need to not have to have this feature.

## Indexing

Asset library indexes do not store previews, this would need to be changed for effiency.

In fact we should considering making a search index for the entire libraries, because the current system is more like a metadata file per .blend, and not optimized for dealing with many .blend files and datablocks. This will become more important for project asset libraries.

An sqlite database per asset library would be a better solution, for efficient queries and incremental updates. This would be a true search index that can be safely deleted and regenerated. Not a file that may be the source of truth for some data, as is the case for current asset index files.

A first iteration of brush assets does not strictly have to have this optimization, particularly if we make an exception for brushes and save them in a single `.blend` file.

## FAQ

* Why no drafts?
  * The naming is wrong, because intent is not to save drafts versions of brushes there, but the finished brushes too.
  * It's additional concept that users have to understand when working with assets, which complicates the user interface and learning process. Other applications do fine without this.
  * Users would have to be aware of two asset folders and two asset libraries, without clear guidance on what to put where.
  * We'd need additional tools in the asset browser to move assets between drafts and other asset libraries.
  * Existing code for drafts can be repurposed to save in another folder location, it's not changing the entire design at the core level.
* Why no override assets?
  * No clear motivation to have them was found.
  * Users having to think about library linking and overrides when editing brushes is too much.
  * Unnecessary code complexity.
* Is an individual .blend file per asset going to be a performance problem?
  * Not if we implement proper indexing, ad solve existing performance problems along with it. The .blend file would only be opened when using the assets, before that it would just read from the index file.
* Why not put all brushes in a single .blend file?
  * For brushes this may be reasonable. For other asset types this would lead to poor performance, for example with heavy meshes using/editing just a single one would require loading/saving all meshes in the asset library. It is better to have a single convention for all asset types.
  * We could still make an exception for brushes and other small datablocks to use a single combined .blend file, if this is preferred.