## General File Formats

* Persistent export settings on collection, for arbitrary file formats including USD
  * Add-ons register file handler that points to operator that supports this
  * User interface to set this up and configure settings
  * Operators to quickly re-export single or all collections in a scene

## USD

* Persistent reference to USD layer in Blender scene hierarchy
  * New datablock type, or sharing a base class with collection datablock
  * Slow USD -> Blender data conversion, using existing implementation
  * Generate datablocks that are read-only, similar to linked datablocks
  * Datablocks are evaluated by dependency graph
* Lazy loading and proxy support for referenced USD layers
  * Show proxy or bounds in viewport, show full contents in render
  * Ideally along with same implementation for linked Blender collections
* Outliner display of contents of referenced USD layers
  * Directly from USD data structures without Blender conversion
* Optimized viewport/render for referenced USD layers
  * Hydra render engine support
  * Cycles support
  * Workbench / Overlay / Selection support
  * Eevee support
* Group object type to replace collection instances
  * When editable, tab into group object and edit its contents
  * Object mode operators modified to support working on this smaller set of objects
* Edit by converting referenced USD layer to Blender objects, and back to USD layer
  * Replacing exisitng USD layer
  * Saved a new USD layer with overrides on existing USD layer
* Edit by directly modifying and saving USD data structures
  * Manual editing of properties
  * Tools and gizmos in viewport
* Scene composition nodes
